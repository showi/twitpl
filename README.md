TwitPl - Twitter Playlist (Beta)
=======

Following medias posted by your friends on twitter and play them!

Qt4/Phonon backend may support a lot of streaming format

* youtube (webm...)
* soundcloud (mp3)

Require
=======
* python 2.7
* python / PyQt4
* python / tweepy
* quvi > 0.9

Note
=======
Developped on Linux, Windows binaries are going to be often outdated! 

Installation
=======
Linux
=====
* Your package manager(apt, pacman...) for **Python2.7** **PyQt4** **quvi** **pip** **tweepy**
* pip install tweepy

Runninng
=====
python2 run.py