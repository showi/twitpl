from twitpl import base_path  # @UnusedImport
from twitpl.version import VERSION, NAME
import sys
import platform
from PyQt4 import QtGui
from PyQt4.QtCore import Qt
splashscreen_enable = True
app = QtGui.QApplication(sys.argv)
if splashscreen_enable:
    splash = QtGui.QSplashScreen()
    splash.setWindowFlags(Qt.WindowStaysOnTopHint | Qt.SplashScreen)
    splash.setPixmap(QtGui.QPixmap(":splashscreen.png"))
    splash.show()
    splash.showMessage('Loading')
    app.processEvents()
if platform.system() == 'Windows':
    QtGui.QApplication.addLibraryPath("contrib/plugins")
app.setApplicationName(NAME)
app.setApplicationVersion(VERSION)
from twitpl.gui.app import TwitPlApp
window = TwitPlApp(app)
if splashscreen_enable:
    splash.finish(window)
mainWindow = window
window.show()
sys.exit(app.exec_())
