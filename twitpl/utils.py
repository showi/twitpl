import logging
log = logging.getLogger('TwitPl:Utils')
import datetime
import os
import traceback


linesep = '-' * 79


def in2bool(value):
    if value is None:
        return False
    elif isinstance(value, bool):
        return value
    elif isinstance(value, str):
        value = value.lower()
        if value.isdigit():
            value = int(value)
            if value == 0:
                return False
            else:
                return True
        if value == 'true':
            return True
        else:
            return False
    if isinstance(value, int):
        if value == 0:
            return False
        else:
            return True
    else:
        raise RuntimeError('Cannot convert input << %s >> to boolean')


def pretty_duration(seconds):
    return str(datetime.timedelta(seconds=int(seconds)))


def readfile(path):
    if not os.path.exists(path):
        log.error("readfile/Error: path doesn't exist <<%s>>", path)
        return None
    out = ''
    with open(path, 'r') as fh:
        line = None
        for line in fh.read(4096):
            out += line
    return out


def trace(exception, limit=None):
    return '%s\n%s\n%s' % (linesep, traceback.format_exc(limit), linesep)
