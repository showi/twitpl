import logging
log = logging.getLogger('TwitPl:ImagesManager')
from PyQt4.QtGui import QPixmap, QIcon
from PyQt4.QtCore import pyqtSignal, QUrl, QObject, QByteArray
from PyQt4.QtNetwork import QNetworkAccessManager, QNetworkRequest


defaultIcon = QIcon(':/images/images/twitter_egg.png')

networkManager = QNetworkAccessManager()


class NetIcon(QPixmap):

    def __init__(self, url, parent=None):
        super(NetIcon, self).__init__()
        self.parent = parent
        self.reply = None
        self.is_loaded = False
        if url is not None or url != '':
            self.url = QUrl(url)
            self.load_url(self.url)
        else:
            self.url = None

    def pixmap(self, size, *a, **ka):
        if not self.is_loaded:
            return defaultIcon.pixmap(size)
        return self.scaled(size, size)

    def load_url(self, url):
        nm = networkManager
        request = QNetworkRequest(url)
        request.setPriority(request.LowPriority)
        request.setAttribute(request.CacheLoadControlAttribute,
                             request.PreferCache)
        reply = nm.get(request)
        reply.finished.connect(self.loaded)
        self.reply = reply

    def loaded(self):
        ba = QByteArray(self.reply.readAll())
        if not self.loadFromData(ba):
            log.error('Cannot load image from url: %s', self.url.toString())
            self.reply.deleteLater()
            return False
        self.is_loaded = True
        if self.parent is not None:
            self.parent.updateSignal.emit()
        self.reply.deleteLater()
        return True


class ImagesManager(QObject):

    updateSignal = pyqtSignal()

    def __init__(self):
        super(ImagesManager, self).__init__()
        self.urls = {}

    def add(self, url):
        if url not in self.urls:
            self.urls[url] = NetIcon(url, parent=self)
        return self.urls[url]

    def __iter__(self):
        for url in self.urls:
            yield self.urls[url]

    def __getitem__(self, url):
        return self.add(url)

    def add_listener(self, listener):
        self.updateSignal.connect(listener)

imagesManager = ImagesManager()
