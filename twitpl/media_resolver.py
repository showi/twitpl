import json
import logging
import os
import platform
system = platform.system()
startupinfo = None
import shlex
import subprocess
if system == 'Windows':
    import _subprocess as _sp  # @bug with python 2.7 ?
    startupinfo = subprocess.STARTUPINFO()
    startupinfo.dwFlags |= _sp.STARTF_USESHOWWINDOW  # @UndefinedVariable
    startupinfo.wShowWindow = _sp.SW_HIDE  # @UndefinedVariable
    #si.creationflags |= subprocess.CREATE_NEW_CONSOLE
from subprocess import check_output, CalledProcessError
from twitpl import base_path
log = logging.getLogger('TwitPl:Quvi')
quvi_paths = None
if platform.system() == 'Windows':
    quvi_bpexe = os.path.join(base_path, 'contrib', 'quvi.exe')
    quvi_paths = [quvi_bpexe]
else:
    quvi_paths = ['/usr/bin/quvi', '/bin/quvi', '/opt/local/bin/quvi']

quvi_bin = None
quvi_dir = None
log.info('Searching quvi binary, %s', quvi_paths)
for path in quvi_paths:
    log.info(' - %s', path)
    if os.path.exists(path):
        quvi_bin = path
        log.info('Using << %s >> binary', quvi_bin)
        break
if quvi_bin is None:
    raise RuntimeError('Could not find << quvi >> binary :(')

quvi_dir = os.path.dirname(quvi_bin)
log.info('Quvi directory: %s', quvi_dir)

from twitpl.utils import trace


class MediaQualityEnum(object):
    default = 0x1
    best = 0x2


class Stream(object):

    def __str__(self):
        s = '<Stream@%i' % id(self)
        for k in self.__dict__:
            s += ' %s="%s"' % (k, self.__dict__[k])
        return s


class MediaURL(object):

    __attributes__ = ['request_url', 'title', 'url', 'thumbnail_url', 'id',
                      'quality', 'is_valid', 'streams', 'current_stream',
                      'host', 'duration']

    def __init__(self, url):
        self.request_url = url
        self.title = None
        self.url = None
        self.thumbnail_url = None
        self.id = None
        self.quality = MediaQualityEnum.default
        self.is_valid = False
        self.streams = []
        self.current_stream = None
        self.host = 'N/A'
        self.duration = None

    def add_stream(self, cls):
        s = cls()
        self.streams.append(s)
        return s

    def set_url(self, url):
        self._url = url

    def get_url(self):
        if self._url is None:
            return self.request_url
        return self._url

    def clone(self):
        media = MediaURL(self.request_url)
        for key in self.__attributes__:
            setattr(media, key, getattr(self, key))
        return media

    url = property(get_url, set_url)


class Plugin(object):

    def handle_media(self, media):
        raise NotImplementedError()

    def resolve(self, media):
        raise NotImplementedError()


class Quvi_1_9_enum_Stream(Stream):
    pass


class Quvi_1_9_enum(Plugin):

    exe = quvi_bin
    name = "Quvi_1_9_enum"
    pre_resolve = False

    cls_stream = Quvi_1_9_enum_Stream

    def __init__(self, url=None):
        pass

    def handle_media(self, _media):
        return True

    def resolve(self, media):
        if media.is_valid:
            log.error("Trying to resolve same media twice...skipping")
            return True
        stdout = None
        args = [self.exe]
        quality = ' '
        if media.quality == MediaQualityEnum.best:
            quality = ' -s best '
        cmd = 'dump -p enum%s -b debug "%s"' % (quality, media.url)
        log.info('CMD: %s', cmd)
        args.extend(shlex.split(cmd))
        log.debug("cmd: %s", cmd)
        try:
            os.chdir(quvi_dir)
            stdout = check_output(args, startupinfo=startupinfo,
                                  stderr=subprocess.STDOUT)
        except CalledProcessError as e:
            log.error("Bad url: %s (%s)\n%s", media.url, e, trace(e))
            return False
        if not self.parse(media, stdout):
            return False
        media.is_valid = True
        return True

    def parse(self, media, stdout):
        media.is_valid = False
        if stdout is None:
            return False
        stdout = stdout.strip()
        if stdout == '':
            return False
        for line in stdout.split('\n'):
            if not self.parse_line(media, line.strip()):
                return False
        return True

    def filter_line(self, _media, line):
        if line.strip() == '':
            return False
        elif line[0] in ('<', '>', '*'):
            return False
        return True

    def split_line(self, _media, line):
        key = None
        value = None
        try:
            key, value = line.split('=', 1)
        except:
            try:
                key, value = line.split(':', 1)
            except:
                return None, None
        if key is not None:
            key = key.strip().lower()
        if value is not None:
            value = value.strip()
        return key, value

    def parse_line(self, media, line):
        if line == '':
            media.add_stream(self.cls_stream)
            return True
        if not self.filter_line(media, line):
            return True
        key, value = self.split_line(media, line)
        if key is None:
            return True
        s_qvm = 'quvi_media_property_'
        s_msp = 'quvi_media_stream_property_'
        if key.startswith(s_qvm):
            key = key[len(s_qvm):]
            return self.set_main_property(media, key, value)
        elif key.startswith(s_msp):
            if media.current_stream is None:
                media.current_stream = media.add_stream(self.cls_stream)
            key = key[len(s_msp):]
            return self.set_stream_property(media, key, value)
        if key in ('host', ):
            return self.set_main_property(media, key, value)
        log.debug('Cannot parse line %s => %s', key, value)
        return True  # ...

    int_keys = ['duration_ms', 'duration']

    def convert_value(self, key, value):
        if key in self.int_keys:
            return int(value)
        return value

    def set_main_property(self, media, key, value):
        value = self.convert_value(key, value)
        if key == 'duration_ms':
            key = 'duration'
            value = value / 1000
        log.info('Set Main %s="%s"', key, value)
        setattr(media, key, value)
        return True

    def set_stream_property(self, media, key, value):
        value = self.convert_value(key, value)
        setattr(media.current_stream, key, value)
        #log.info('Set Stream %s="%s"', key, value)
        return True


class MediaResolver(object):

    def __init__(self):
        self.plugins = []
        self.plugins.append(Quvi_1_9_enum())

    def resolve(self, media):
        if not isinstance(media, MediaURL):
            media = MediaURL(media)
        for plugin in self.plugins:
            if plugin.handle_media(media):
                if plugin.resolve(media):
                    return media
        return None

media_resolver = MediaResolver()
