import json
import logging
import os
import platform
import re
# from urlparse import urlparse
system = platform.system()
startupinfo = None
import shlex
if system == 'Windows':
    import subprocess
    import _subprocess as _sp  # @bug with python 2.7 ?
    startupinfo = subprocess.STARTUPINFO()
    startupinfo.dwFlags |= _sp.STARTF_USESHOWWINDOW  # @UndefinedVariable
    startupinfo.wShowWindow = _sp.SW_HIDE  # @UndefinedVariable
    #si.creationflags |= subprocess.CREATE_NEW_CONSOLE
from subprocess import check_output, CalledProcessError, STDOUT
from twitpl import base_path
log = logging.getLogger('TwitPl:Quvi')
quvi_paths = None
if platform.system() == 'Windows':
    quvi_bpexe = os.path.join(base_path, 'contrib', 'quvi.exe')
    quvi_paths = [quvi_bpexe]
else:
    quvi_paths = ['/usr/bin/quvi', '/bin/quvi']

quvi_bin = None
quvi_dir = None
log.info('Searching quvi binary, %s', quvi_paths)
for path in quvi_paths:
    log.info(' - %s', path)
    if os.path.exists(path):
        quvi_bin = path
        log.info('Using << %s >> binary', quvi_bin)
        break
if quvi_bin is None:
    raise RuntimeError('Could not find << quvi >> binary :(')

quvi_dir = os.path.dirname(quvi_bin)
log.info('Quvi directory: %s', quvi_dir)


def is_not_empty_string(data):
    if data is None or data == '':
        return False
    if not isinstance(data, str):
        return False
    return True


class QuviStream(object):
    def __init__(self):
        self.url = None
        self.container = None


class QuviMediaBase(object):

    def __init__(self):
        self.streams = []

    def add_stream(self, stream):
        self.streams.append(stream)

    def __str__(self):
        s = '<QuviMedia '
        for key in self.keys:
            s += ' %s="%s"' % (key, getattr(self, key))
        s += '>'
        return s


class QuviMediaRfc2483(QuviMediaBase):

    tagstart_media = 'quvi_media_property_'
    tagstart_stream = 'quvi_media_stream_property_'
    tagstart_host = '* Found bundle for host'
    keys = ['url', 'host', 'thumbnail_url', 'title', 'id',
                        'start_time_ms', 'duration_ms', 'duration',
                        'page_title', 'page_url', 'extracted_id']

    def __init__(self, url):
        super(QuviMediaRfc2483, self).__init__()
        self.url = url
        self.host = None
        self.thumbnail_url = None
        self.title = None
        self.id = None
        self.start_time_ms = None
        self.duration_ms = None
        tag = self.tagstart_host.replace('*', '\*')
        pattern = '^%s\s+([^\s]+):.*$' % tag
        log.info('Pattern: %s', pattern)
        self.regex_host = re.compile(pattern, re.IGNORECASE)

    @property
    def duration(self):
        pass

    @duration.getter
    def duration(self):
        dms = self.duration_ms
        if dms is None or dms == 0:
            return 0
        return int(dms) / 1000

    @property
    def page_url(self):
        return self.url

    @page_url.getter
    def page_url(self):
        return self.url

    @property
    def page_title(self):
        return self.title

    @page_title.getter
    def page_title(self):
        return self.title

    @property
    def extracted_id(self):
        return self.id

    @extracted_id.getter
    def extracted_id(self):
        return self.id

    def parse(self, sin):
        if not is_not_empty_string(sin):
            log.error('Cannot parse data, empty or not a string: <<%s>>')
            return False
        stream = QuviStream()
        for line in sin.split('\n'):
            if not is_not_empty_string(line):
                log.info('new stream data')
                if stream is not None:
                    self.add_stream(stream)
                stream = QuviStream()
            if line.startswith(self.tagstart_host):
                match = self.regex_host.match(line)
                if match:
                    host = match.group(1)
                    self.host = host.lower().strip()
                log.info('Host: %s', self.host)
            elif line.startswith('Host:'):
                self.host = line[len('Host: '):].strip()
            try:
                key, value = line.split('=', 1)
            except:
                log.error('Cannot parse line: %s', line)
                continue
            key = key.lower()
            if key.startswith(self.tagstart_media):
                key = key[len(self.tagstart_media):]
                log.info('Setting %s: %s', key, value)
                setattr(self, key, value)
            elif key.startswith(self.tagstart_stream):
                key = key[len(self.tagstart_stream):]
                log.info('Setting stream %s: %s', key, value)
                setattr(stream, key, value)
        log.info('Parsed: %s', self)
        return True


class Quvi(object):

    exe = quvi_bin

    def __init__(self, url=None):
        self.version = 'stable'
        self.is_valid = False
        self.data = None
        self.url = url
        self.quality = 'default'  # [best, default]

    @property
    def url(self):
        self._url

    @url.setter
    def url(self, url):
        self.is_valid = False
        self.data = None
        self._url = url
        if url is not None:
            self.validate()

    @url.getter
    def url(self):
        return self._url

    def __getattr__(self, key):
        if self.data is None:
            return None
        if key not in self.data.keys:
            log.error('Invalid key <<%s>>', key)
            return None
        return getattr(self.data, key)

    def validate(self):
        if self.version == 'unstable':
            return self.validate_unstable()
        return self.validate_stable()

    def validate_unstable(self):
        self.is_valid = False
        self.data = None
        sin = None
        args = [self.exe]
        quality = ''
        if self.quality == 'best':
            quality = ' -s best'
        cmd = 'dump -b debug%s %s 2>&1' % (quality, self.url)
        args.extend(shlex.split(cmd))
        log.debug('cmd: %s', cmd)
        try:
            os.chdir(quvi_dir)
            sin = check_output(args, startupinfo=startupinfo, stderr=STDOUT)
        except CalledProcessError as e:
            log.error("Bad url: %s (%s)", self.url, e)
            return False
        data = QuviMediaRfc2483(self.url)
        if not data.parse(sin):
            return False
        self.data = data
        self.is_valid = True
        return True

    def validate_stable(self):
        self.is_valid = False
        self.data = None
        out = None
        args = [self.exe]
        cmd = '--verbosity=quiet -f %s "%s"' % (self.quality, self.url)
        args.extend(shlex.split(cmd))
        log.debug("cmd: %s", cmd)
        try:
            os.chdir(quvi_dir)
            out = check_output(args, startupinfo=startupinfo)
        except CalledProcessError as e:
            log.error("Bad url: %s (%s)", self.url, e)
            return False
        if not self.__parse_json_old(out):
            self.data = None
            return False
        self.is_valid = True
        return True

    def __parse_json_old(self, string):
        if string is None:
            return False
        try:
            self.data = json.loads(string)
        except Exception as e:
            log.error('Could not parse json data, Error: %s', e)
            return False
        return True

    def __str__(self):
        s = '<TwitPl::Quvi host:%s extracted_id: %s>' % (self.host,
                                                         self.extracted_id)
        return s

if __name__ == '__main__':
    q = Quvi()
    urls = ['http://t.co/DP17TP5Ul7', 'https://pouueet.nz/zd2',
            'https://t.co/x4dtzYzwfR']
    for url in urls:
        q.url = url
        if q.is_valid:
            print("-" * 80)
            print(q.data)
        break
