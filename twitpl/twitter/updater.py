import logging
logging.basicConfig()
log = logging.getLogger('TwitPl:Updater')
log.setLevel(logging.ERROR)
from os import getpid
from PyQt4.QtCore import QThread
from time import sleep, time
from twitpl.db.init import dbo
from twitpl.twitter.api import api
from twitpl.media_resolver import media_resolver as mr
from twitpl.utils import in2bool

MAX_COUNT = 50
MIN_UPDATE_DELTA = 1
MAX_UPDATE_DELTA = 15


class Updater(QThread):

    def __init__(self, daemon=True):
        self.started_on = time()
        QThread.__init__(self)
        self.dbo = dbo
        self.daemon = daemon
        self.api = api
        self.update_on = time() - MAX_UPDATE_DELTA - 1

    def run(self):
        """Thread main loop
        """
        while True:
            elapsed = time() - self.update_on
            self.update_on = time()
            diff = MIN_UPDATE_DELTA - elapsed
            if diff > 0:
                log.info('We are updating too fast, sleeping %ss', elapsed)
                sleep(abs(diff + 0.5))
            self.update()

    def update(self):
        """
        """
        log.info('Updating... ThreadID %s', getpid())
        usersModel = self.dbo.model.users
        if self.api.api is None:
            log.error('We are not authenticated')
            return False
        if usersModel.is_empty():
            log.error('No user in database')
            return False
        for user in usersModel.list():
            self.update_user(user)
        for user in usersModel.list():
            self.fetch_user_tweets(user)
        return True

    def update_user(self, user):
        user_id = str(user.value('id').toString())
        is_active = in2bool(user.value('is_active').toInt()[0])
        if not is_active:
            log.info('Skipping inactive user, id: %s', user_id)
            return False
        usersModel = self.dbo.model.users
        ret, old = usersModel.get(user_id)
        if ret is False:
            log.error("Cannot update invalid user <<%s>>", user_id)
            return False
        old_count = old.value('statuses_count').toInt()[0]
        new_user = self.api.get_user(user_id)
        new_count = int(new_user.statuses_count)
        log.info("NEW COUNT: %s, OLD COUNT: %s", new_count, old_count)
        need_update = False
        if old_count != new_count:
            need_update = True
        user_name = new_user.screen_name
        if not need_update:
            log.info('no update for user <<%s>>', user_name)
            return True
        log.info('updating user <<%s>', user_name)
        return usersModel.update(new_user)

    def fetch_user_tweets(self, user):
        user_name = str(user.value('screen_name').toString())
        tweetsModel = self.dbo.model.tweets
        user_id = str(user.value('id').toString())
        count = int(user.value('statuses_count').toInt()[0])
        db_count = tweetsModel.user_count(user_id)
        log.info('[%s] tweets count: %s / %s', user_name, db_count, count)
        if count == db_count:
            log.info('no more tweets for <<%s>>', user_name)
            return False
        if db_count == 0:
            log.info('get first tweets for <<%s>>', user_name)
            statuses = self.api.user_timeline(user_id, count=MAX_COUNT,
                                              exclude_replies=True)
            return self.parse_statuses(user, statuses)
        last_id = tweetsModel.user_last_id(int(user_id)) + 1
        if last_id is not None:
            log.info('get newest tweets for <<%s>', user_name)
            statuses = self.api.user_timeline(user_id, count=MAX_COUNT,
                                              exclude_replies=True,
                                              since_id=last_id)
            if self.parse_statuses(user, statuses):
                return True
        first_id = tweetsModel.user_first_id(user_id) - 1
        log.info('get old tweet for <<%s>>', user_name)
        statuses = self.api.user_timeline(user_id, count=MAX_COUNT,
                                          exclude_replies=True,
                                          max_id=first_id)
        return self.parse_statuses(user, statuses)

    def parse_statuses(self, user, statuses):
        tweetsModel = self.dbo.model.tweets
        if not user:
            raise RuntimeError('user cannot be None')
        if statuses is None or len(statuses) == 0:
            return False
#         user_id = str(user.value('id').toString())
        user_name = str(user.value('screen_name').toString())
        ok = False
        log.info('parsing user tweets for <<%s>', user_name)
        for status in statuses:
            tweetsModel.add(user, status)
#             if tweetsModel.exists(status.id):
#                 log.info('tweets already saved <<%s>>', status.id)
#                 continue
#             tweetsModel.add(user_id, status)
#             hashtags = [h['text'] for h in status.entities['hashtags']]
#             self.add_hashtags(status.id, hashtags)
#             urls = [u['expanded_url'] or u['url']
#                     for u in status.entities['urls']]
#             ok = self.add_medias(user, status.id, urls)
        return ok

    def add_hashtags(self, id_tweet, hashtags):
        hashtagsModel = self.dbo.model.hashtags
        tweetsModel = self.dbo.model.tweets
        ids = hashtagsModel.add(hashtags)
        log.info('ids: %s', ids)
        tweetsModel.attach_hashtags(id_tweet, ids)

    def add_medias(self, user, id_tweet, urls):
        mediasModel = self.dbo.model.medias
        tweetsModel = self.dbo.model.tweets
        quvi = self.quvi
        ok = False
        for url in urls:
            media = mr.resolve(url)
            if not media:
                continue
            if mediasModel.add(media):
                ok = True
                ret, record = mediasModel.get(quvi.host, quvi.extracted_id)
                if ret:
                    id_media = record.value('id').toString()
                    if tweetsModel.attach_media(id_tweet, id_media):
                        pass
        return ok
