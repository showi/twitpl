import logging
logging.basicConfig()
log = logging.getLogger('TwitPl:Api')
from time import time, sleep
import tweepy
from tweepy.error import TweepError
from twitpl.gui.webDialog import TwitPlWebDialog
from twitpl.conf import conf, ck_key, ck_secret
from twitpl.utils import  trace

SLEEP = 5
MAX_REQUEST = 350 / 60 / 60


def extract_hash_tags(s):
    return set(part[1:].lower() for part in s.split() if part.startswith('#'))


class Api(object):

    def __init__(self, parent=None):
        self.key = b'gHtUTgeNBRVXqBLKeTGALA'
        self.secret = b'rYU6zWCxlSk1QmVEAPxIZ7GORb6rpB3WULajT7ugTM'
        self.api = None
        self.session_token = None
        self.parent = parent
        self.num_request = 0
        self.started_on = time()

    def authenticate_save(self, key, secret):
        log.info('Saving token <%s>/<%s>', key, secret)
        conf[ck_key] = key
        conf[ck_secret] = secret
        log.info('Key: %s', conf[ck_key])
        log.info('Secret: %s', conf[ck_secret])
        conf.write()

    def rate_limiting(self):
        self.num_request += 1
        elapsed = time() - self.started_on
#         elapsed /= 1000
        #elapsed = 60.0
        avg = self.num_request / elapsed
        log.debug('Elapsed: %s, Rate avg: %s', elapsed, avg)
        if avg > MAX_REQUEST:
            log.debug('Sleeping %s!', SLEEP)
            sleep(SLEEP)

    def __getattr__(self, key, *a, **ka):
        """ Forward unknow method to self.api """
        if self.api is None:
            log.error('api is None, need authentication?')
            return None
#             raise RuntimeError('Api not set')
        try:
            self.rate_limiting()
            return getattr(self.api, key, *a, **ka)
        except TweepError as e:
            log.error('TweepError: %s, %s', e.code, trace(e))
        except Exception as e:
            log.error('Something went wrong: %s', e)
        return None

    def authentication_remove(self):
        conf.authentication_remove()
        self.api = None

    def authenticate_restore(self):
        if self.api is not None:
            return True
        key = None
        secret = None
        try:
            key = conf.get(ck_key)
            secret = conf.get(ck_secret)
            log.info('key/secret %s/%s', key, secret)
        except Exception as e:
            log.error('Error while reading configuration: %s\n%s', e, trace(e))
            raise e
        if key is None or key == '':
            return False
        if secret is None or secret == '':
            return False
        log.info('Restoring %s/%s', key, secret)
        auth = tweepy.OAuthHandler(self.key, self.secret)
        auth.set_access_token(key, secret)
        api = tweepy.API(auth)
        try:
            name = api.me().name
            log.info("User: %s", name)
        except Exception as e:
            log.error('%s\n%s', e, trace(e))
            return False
        self.api = api
        return True

    def authenticate(self):
        if self.authenticate_restore():
            log.info('We are already authenticated')
            return True
        self.api = None
        auth = tweepy.OAuthHandler(self.key, self.secret)
        redirect_url = None
        try:
            redirect_url = auth.get_authorization_url()
        except tweepy.TweepError as e:
            log.error('Error! Failed to get request token, %s\n%s', e,
                      trace(e))
            return False
            #raise e
        d = TwitPlWebDialog(redirect_url, parent=self.parent)
        d.setModal(True)
        d.exec_()
        verifier = d.lineEditPin.text()
        log.info('CodePin: %s', verifier)
        d.deleteLater()
        auth.set_request_token(auth.request_token.key,
                               auth.request_token.secret)
        try:
            auth.get_access_token(verifier)
        except tweepy.TweepError:
            log.error('Error! Failed to get access token')
            return False
        self.authenticate_save(auth.access_token.key, auth.access_token.secret)
        api = tweepy.API(auth, cache=tweepy.MemoryCache(timeout=30))
        if not api:
            return False
        self.api = api
        try:
            self.api.me()
        except Exception as e:
            log.error('Error: %s', e)
            return False
        return True

    def user_exist(self, name):
        if name is None:
            return None
        name = name.strip()
        if name == '':
            return None
        log.info('Testing user: %s', name)
        return self.api.get_user(name)

api = Api()
