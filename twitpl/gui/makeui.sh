#!/bin/sh
echo `which pyuic4`
uifiles="TwitplGuiMain TwitPlGuiWebDialog"
echo "* Converting ui files to python Object"
ret=0
for name in $uifiles; do
	pyuic4 -w -o "$name.py" -x "$name.ui"
	if [ $? != 0 ]; then
		echo " [-] $name.ui -> $name.py"
		ret=1
	else
		echo " [+] $name.ui -> $name.py"
	fi
done
exit $ret

