import logging
log = logging.getLogger('TwitPl:Gui')
from PyQt4 import QtGui
from PyQt4.QtCore import QUrl
try:
    from PyQt4.QtCore import QString
except ImportError:
    # we are using Python3 so QString is not defined
    QString = str

from twitpl.gui.TwitPlGuiWebDialog import Ui_Dialog


class TwitPlWebDialog(QtGui.QDialog, Ui_Dialog):

    def __init__(self, url, parent=None):
        self.url = url
        QtGui.QMainWindow.__init__(self, parent)
        Ui_Dialog.__init__(self)
        self.setupUi(self)
        self.webView.setUrl(QUrl(self.url))
        self.lineEditAuthUrl.setText(QString(self.url))

if __name__ == '__main__':
    d = TwitPlWebDialog()
    d.show()
