import logging
from twitpl.conf import conf
log = logging.getLogger('TwitPl:Gui')
try:
    from urllib.parse import unquote_plus  # @UnresolvedImport @UnusedImport
except:
    from urllib import unquote_plus  # @Reimport @UnusedImport

from PyQt4 import QtGui
from PyQt4.QtCore import Qt, QTimer, QModelIndex
from PyQt4.QtGui import QItemSelectionModel
try:
    from PyQt4.phonon import Phonon
except Exception as e:
    log.critical(e)
from twitpl.gui.TwitPlGuiMain import Ui_MainWindow
from twitpl.twitter.api import api
# from twitpl.media_resolver import media_resolver = mr
from twitpl.version import DEBUG
from twitpl.player import Player
from twitpl.db.init import dbo as dbObject
from twitpl.twitter.updater import Updater
from twitpl.utils import trace


class MediaItemSelectionModel(QItemSelectionModel):
    pass


tbl_view = {
    'users': 'tableViewUsers',
    'medias': 'tableViewPlaylist',
    'hashtags': 'tableViewHashtags'
}


class TwitPlApp(QtGui.QMainWindow, Ui_MainWindow):

    def __init__(self, _app):
        QtGui.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.player = Player(self)
        self.__restore_geometry()
        self.api = api
        self.selected_idx = None
        self.dbo = dbObject
        self.configureMenu()
        self.configure_viewUsers()
        self.configure_viewHashtags()
        self.configure_viewPlaylist()
        self.configure_mediaPlayer()
        self.configure_filter()
        self.playlist_need_refresh = 1
        self.index = QModelIndex()
        """ Add / remove Button """
        self.pushButtonUserAdd.clicked.connect(self.__user_add)
        self.pushButtonUserDel.clicked.connect(self.__user_del)
        self.__defered_init()

    def __restore_geometry(self):
        geo = conf['mainWindowGeometry']
        if geo.isNull():
            return False
        try:
            geo = geo.toByteArray()
        except:
            pass
        self.restoreGeometry(geo)
        return True

    def __defered_init(self):
#         if DEBUG:
#             self.remove_auth()
        #self.configure_timer()
        self.api.authenticate()
        self.updater = Updater()
        self.updater.start()
        if DEBUG:
            self.__user_add('JaSndBwoy')

    def configure_timer(self):
        timer = QTimer(self)
        timer.timeout.connect(self.update_views)
        timer.start(5000)

    def get_view(self, key):
        if key not in tbl_view:
            raise KeyError('Invalid view <<%s>>' % key)
        return getattr(self, tbl_view[key])

    def configureMenu(self):
        self.actionSettingsDeleteAuth.triggered.connect(self.remove_auth)

    def remove_auth(self):
        self.api.authentication_remove()

    def playlist_got_update(self, *_a, **_ka):
        self.blockSignals(True)
        try:
            self.update_views()
        except Exception as e:
            log.error('Cannot update playlist, %s\n%s', e, trace(e))
        finally:
#             self.blockSignals(False)
            timer = QTimer(self)
            timer.timeout.connect(self.unblock_signal)
            timer.setSingleShot(True)
            timer.start(1)

    def unblock_signal(self):
        self.blockSignals(False)

    def update_views(self):
        view = self.tableViewPlaylist
        self.dbo.model.mediasViewModel.setQuery()
        row = None
        try:
            row = self.player.index.row()
            view.selectRow(row)
        except Exception as e:
            log.error('Cannot restore selected index(%s), %s\n%s', row,
                      e, trace(e))

    def configure_viewUsers(self):
        """ Bind UserView with our model """
        tbl = self.get_view('users')
        model = self.dbo.model.users
        tbl.setModel(model)
        tbl.setShowGrid(False)
        tbl.resizeColumnsToContents()
        tbl.resizeRowsToContents()
        tbl.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        tbl.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        model.dataChanged.emit(QModelIndex(), QModelIndex())

    def configure_viewHashtags(self):
        """ Bind UserView with our model """
        hashtagsModel = self.dbo.model.hashtags
        tbl = self.get_view('hashtags')
        tbl.setModel(hashtagsModel)
        tbl.setShowGrid(False)
        tbl.resizeColumnsToContents()
        tbl.resizeRowsToContents()
        tbl.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        tbl.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)

    def configure_viewPlaylist(self):
        """ Bind UserView with our model """
        view = self.get_view('medias')
        proxy = self.dbo.model.mediasProxy
        wmodel = self.dbo.model.medias
        wmodel.rowsInserted.connect(self.playlist_got_update)
        model = self.dbo.model.mediasViewModel
        proxy.setSourceModel(model)
        view.setModel(proxy)
        view.show()
        view.clicked.connect(self.player.play)

    def configure_mediaPlayer(self):

#         self.player.attach('duration', self.label)
        cbo = self.comboBoxOutputDevice
        backendCapabilities = Phonon.BackendCapabilities
        cbo.clear()
        current = Phonon.AudioOutputDevice().name()
        idx = 0
        for device in backendCapabilities.availableAudioOutputDevices():
            name = device.name()
            cbo.addItem(device.name())
            if name == current:
                cbo.setCurrentIndex(idx)
            idx += 1
        cbo.setDisabled(True)
        self.player.attach('quality', self.comboBoxMediaQuality)
        self.comboBoxMediaQuality.currentIndexChanged.connect(
                                            self.player.on_quality_changed)
        frame = self.framePlayer
        self.playerSlider = self.player.ui_seek_slider()
        self.playerStatus = QtGui.QLabel(self)
        self.playerPlay = QtGui.QPushButton(self)
        #self.playerPlay.setDisabled(True)
        self.playerPlay.clicked.connect(self.player.toggle_play)
        self.playerVolumeSlider = self.player.ui_volume_slider()
        layout = QtGui.QVBoxLayout(frame)
        uframe = QtGui.QFrame(self)
        dframe = QtGui.QFrame(self)
        layout.addWidget(uframe)
        layout.addWidget(dframe)
        layout = QtGui.QHBoxLayout(uframe)
        layout.addWidget(self.playerPlay)
        layout.addWidget(self.playerSlider)
        layout.addWidget(self.playerStatus)
        layout = QtGui.QHBoxLayout(dframe)
        layout.addWidget(self.playerVolumeSlider)
        layout = QtGui.QHBoxLayout(self.frameVideo)
        layout.addWidget(self.player.videoWidget)

        self.player.attach('duration', self.playerStatus)
        self.player.attach('title', self.labelMusicTitle)
        self.player.attach('play_text', self.playerPlay)
        self.player.attach('viewWidget', self.tableViewPlaylist)
        self.player.attach('buffer_status', self.bufferProgressBar)

    def configure_filter(self):
        self.lineEditFilter.editingFinished.connect(self.update_filter)

    def update_filter(self):
        model = self.dbo.model.mediasViewModel
        keywords = str(self.lineEditFilter.text()).split(' ')
        hashtags = []
        for k in keywords:
            if k.startswith('#'):
                hashtags.append(k[1:])
            elif k in ('OR', 'AND', 'NOT'):
                hashtags.append(k)
        hashtags = ' '.join(hashtags)
        model.setQuery(hashtags)
        return

    def closeEvent(self, event):
        log.info('Saving geometry')
        conf['mainWindowGeometry'] = self.saveGeometry()
        conf['mainWindowState'] = self.saveState()
        super(TwitPlApp, self).closeEvent(event)

    def play_next(self):
            model = self.dbo.model.mediasViewModel
            view = self.tableViewPlaylist
            index = view.selectedIndexes()[0]
            try:
                view.setCurrentIndex(index.sibling(index.row() + 1, 0))
                index = view.selectedIndexes()[0]
            except IndexError as e:
                log.error('Error (end of playlist ?): %s', e)
                view.setCurrentIndex(model.index(0, 0))
                index = view.selectedIndexes()[0]
            self.playClicked(index)

    def api_authenticate(self):
        return self.api.authenticate()

    def __user_add(self, name=None):
        usersModel = self.dbo.model.users
        if not self.api_authenticate():
            return False
        log.info('Name: %s', name)
        if name is None or not isinstance(name, str):
            name, ok = QtGui.QInputDialog.getText(self, 'Add user',
                                                  'Username/ScreenName:')
            if ok is False:
                return False
            if name.isNull():
                return False
            name = name.trimmed()
            if name.isEmpty():
                return False
            name = str(name)
        user = self.api.user_exist(name)
        if not user:
            log.error('Invalid tweeter user << %s >>', user)
            return False
        usersModel.add(user)

    def __user_del(self):
        usersModel = self.dbo.model.users
        indexes = self.tableViewUsers.selectedIndexes()
        if len(indexes) < 1:
            return False
        idx = indexes[0].row()
        log.info("Removing user: %s", idx)
        if usersModel.removeRow(idx):
            usersModel.submitAll()
            return True
        usersModel.reset()
        return False
