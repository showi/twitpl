from PyQt4.QtGui import QDialog
from PyQt4.QtGui import QApplication
from PyQt4.QtWebKit import QWebView
from PyQt4.QtCore import QUrl


class WebDialog(QDialog):

    def __init__(self, url, parent=None):
        self.url = url
        super(WebDialog, self).__init__(parent)
        self.web = QWebView(self)
        self.web.load(QUrl(self.url))
        self.web.show()


if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    w = WebDialog('http://google.com')
    w.show()
    sys.exit(app.exec_())
