#!/bin/sh

inf="ressources.qrc"
ouf="ressources_rc.py"
pyrcc4 -py3 ${inf} -o ${ouf}
echo "* Converting ressources"
if [ $? != 0 ]; then
	echo " [-] fail to make ${inf}"
	exit 1
else
	echo " [+] ${inf} -> ${ouf}"
fi
exit 0
 
