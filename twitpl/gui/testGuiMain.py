#!/usr/bin/env python
import sys

from PyQt4 import QtGui, uic


app = QtGui.QApplication(sys.argv)
QtGui.QApplication.setStyle(QtGui.QStyleFactory.create("Cleanlooks"))
widget = uic.loadUi("TwitplGuiMain.ui")
widget.show()

app.exec_()
