import os
import sys
import logging


class StreamToLogger(object):
    """
    Fake file-like stream object that redirects writes to a logger instance.
    """
    def __init__(self, logger, log_level=logging.INFO):
        self.logger = logger
        self.log_level = log_level
        self.linebuf = ''

    def write(self, buf):
        for line in buf.rstrip().splitlines():
            self.logger.log(self.log_level, line.rstrip())

base_path = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                         os.path.pardir))

from twitpl.version import LOGLVL, REDIRECTLOG

logging.basicConfig(
   level=LOGLVL,
   format='%(asctime)s -%(levelname)s:%(name)s >> %(message)s',
   datefmt='%H:%M:%S',
   #filename=os.path.join(base_path, "twitpl.log"),
   #filemode='w'
)
if REDIRECTLOG:
    stdout_logger = logging.getLogger('STDOUT')
    sl = StreamToLogger(stdout_logger, logging.INFO)
    sys.stdout = sl

    stderr_logger = logging.getLogger('STDERR')
    sl = StreamToLogger(stderr_logger, logging.ERROR)
    sys.stderr = sl


log = logging.getLogger('TwitPl:Init')
log.info('BasePath: %s', base_path)
