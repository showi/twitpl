import logging
log = logging.getLogger('TwitPl:Dbo')
log.setLevel(logging.ERROR)

import os
from twitpl import base_path
from twitpl.db.connect import Db
from twitpl.db.model.all import UsersModel, HashtagsModel, TweetsModel
from twitpl.db.model.all import MediasModel, MediasModelProxy, MediasViewModel
from os import getpid
_key_model = {
    'users': UsersModel,
    'hashtags': HashtagsModel,
    'tweets': TweetsModel,
    'medias': MediasModel,
    'mediasProxy': MediasModelProxy,
    'mediasViewModel': MediasViewModel
}


class key2models(object):
    def __init__(self, dbo):
        self.dbo = dbo
        self.data = {}
        self.connection = None

    def __getattr__(self, key):
        if not self.dbo.is_open():
            raise RuntimeError('Cannot get model, database not open')
        if key not in _key_model:
            raise KeyError('Invalid key <<%s>>', key)
        if not key in self.data:
            obj = _key_model[key]
            self.data[key] = obj(db=self.dbo.connection.handle)
        return self.data[key]


class Dbo(object):

    def __init__(self, name='default'):
        self.name = name
        self.path = os.path.join(base_path, 'data', 'twitpl.db')
        self.connection = None
        self.model = None

    def create_database(self):
        if not self.is_open():
            log.error('Database is not open')
            return False
        if self.connection.load_schema():
            log.error('Cannot create tables!')
            return False
        return True

    def open(self):
        log.info('THREAD IDENT: %s', getpid())
        if self.is_open():
            self.close()
        connection = Db(path=self.path, name=self.name)
        if not connection.open():
            log.error("Cannot open database: %s", self.path)
            return False
        self.connection = connection
        self.model = key2models(self)
        return True

    def is_open(self):
        if self.connection is not None:
            return True
        return False

    def close(self):
        if self.is_open():
            self.connection.close()
            self.connection = None
        self.model = {}
