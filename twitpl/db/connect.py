import logging
log = logging.getLogger('TwitPl:Db:Connect')
log.setLevel(logging.ERROR)
import os
from PyQt4 import QtSql
from PyQt4.QtSql import QSqlQuery
from twitpl import base_path
from twitpl.utils import readfile


class Db(object):

    def __init__(self, path, name='default'):
        self.name = name
        self.path = path
        self.handle = None

    def open(self, path=None):
        self.close()
        if path is None:
            path = self.path
        else:
            self.path = path
        log.info('Opening SqlLite Database: %s', path)
        handle = QtSql.QSqlDatabase.addDatabase('QSQLITE', self.name)
        handle.setDatabaseName(path)
        if not handle.open():
            return False
        self.handle = handle
        return True

    def exec_query(self, query):
        if self.handle is None:
            log.error('Database not open')
            return False
        self.last_error = None
        log.info("Executing query: %s", query)
        query = QSqlQuery(query, self.handle)
        if not query.exec_():
            query.clear()
            msg = query.lastError().text()
            if msg == '':
                log.error('Error: %s', query.lastError().text())
            return False
        query.clear()
        return True

    def iter_schemas(self, path):
        for dirpath, _dirnames, filenames in os.walk(path):
            for filename in filenames:
                if not filename.endswith('.sql'):
                    continue
                yield os.path.join(dirpath, filename)

    def load_schema(self):
        openit = False
        if not self.is_open():
            if not self.open():
                log.error('Cannot open database')
                return False
            openit = True
        schema_path = os.path.join(base_path, 'twitpl', 'db', 'schema')
        ok = True
        for schema in self.iter_schemas(schema_path):
            msg = "loading schema: %s" % (schema)
            if not self.exec_query(readfile(schema)):
                ok = False
                log.error('[%s] %s', '-', msg)
            else:
                log.info('[%s] %s', '+', msg)
        if openit:
            self.close()
        return ok

    def close(self):
        if self.is_open():
            self.handle.close()
            self.handle = None

    def is_open(self):
        if self.handle is None:
            return False
        return True


def transaction(f):
    """ Unused
    """
    def wrapper(self, *a, **ka):
        return f(self, *a, **ka)
    return wrapper
