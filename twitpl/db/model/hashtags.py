import logging
log = logging.getLogger('TwitPl:Model:Hashtags')
log.setLevel(logging.ERROR)

from PyQt4.QtSql import QSqlQuery, QSqlTableModel
from PyQt4.Qt import Qt
from twitpl.db.connect import transaction


class HashtagsModel(QSqlTableModel):

    def __init__(self, parent=None, db=None):
        super(HashtagsModel, self).__init__(parent, db)
        self.db = db
        self.setTable('hashtags')
        self.setEditStrategy(QSqlTableModel.OnManualSubmit)
        if not self.select():
            log.error('HashtagsModel Error: %s', self.lastError().text())

    @transaction
    def updateSlot(self):
        self.select()

    def headerData(self, section, orientation, role):
        if role != Qt.DisplayRole:
            return super(HashtagsModel, self).headerData(
                                                section, orientation, role)
        result = None
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            try:
                result = ('#')[section]
            except IndexError:
                pass
        return result

    def columnCount(self, index):
        return 1

    def data(self, index, role):
        result = None
        if not index.isValid():
            return result
        if role != Qt.DecorationRole and role != Qt.DisplayRole:
            return super(HashtagsModel, self).data(index, role)
        row = index.row()
        col = index.column()
        record = self.record(row)
        if role == Qt.DecorationRole:
            return result
        elif role == Qt.DisplayRole:
            if col == 0:
                result = record.value('text').toString()
        return result

    @transaction
    def get(self, hashtag, colname='id'):
        if colname not in ['id', 'text']:
            raise ValueError('Invalid <<colname=%s>> must be <id> or <text>')
        query = QSqlQuery(self.db)
        query.prepare("SELECT id FROM hashtags WHERE %s=:hashtag" % colname)
        query.bindValue(':hashtag', hashtag)
        if not query.exec_():
            log.error('Cannot get hashtag <<hashtag>>\n\tError: %s',
                      query.lastError().text())
            query.clear()
            return False, None
        if not query.first():
            log.error("Error (no next): %s", query.lastError().text())
            query.clear()
            return False, None
        hid = query.value(0).toInt()
        if not hid[1]:
            log.error('Error: Cannot convert value to int')
            return False, None
        return True, hid[0]

#     @transaction
    def add(self, hashtags):
        ids = []
        log.info('Trying to insert hashtags << %s >>', hashtags)
        for hashtag in hashtags:
            record = self.record()
            record.setValue('text', hashtag)
            if not self.insertRecord(-1, record):
                log.error('Cannot insert hashtag <<%s>\n\tError: %s',
                          hashtag, self.lastError().text())
                continue
            if not self.submitAll():
                log.debug('Cannot insert hashtag <<%s>>\n\tError: %s',
                          hashtag, self.lastError().text())
                self.revertAll()
        for hashtag in hashtags:
            ret, hid = self.get(hashtag, colname='text')
            if ret:
                ids.append(hid)
        return ids
