import logging
log = logging.getLogger('TwitPl:Model:Users')
log.setLevel(logging.ERROR)
from PyQt4.QtSql import QSqlQuery, QSqlTableModel
from PyQt4.Qt import Qt
from twitpl.db.connect import transaction
from twitpl.utils import in2bool
from twitpl.images_manager import imagesManager


class UsersModel(QSqlTableModel):

    fields = ['id', 'created_at', 'description', 'name', 'screen_name',
              'statuses_count', 'profile_image_url', 'is_active']

    def __init__(self, parent=None, db=None):
        super(UsersModel, self).__init__(parent=parent, db=db)
        self.db = db
        self.setTable('users')
        self.setEditStrategy(QSqlTableModel.OnManualSubmit)
        if not self.select():
            log.error("UsersModel Error: %s", self.lastError().text())

    @transaction
    def updateSlot(self):
        self.select()

    def flags(self, index):
        col = index.column()
        flags = super(UsersModel, self).flags(index)
        if col != 0:
            return flags
        return flags | Qt.ItemIsUserCheckable

    def headerData(self, section, orientation, role):
        if role != Qt.DisplayRole:
            return super(UsersModel, self).headerData(
                                                section, orientation, role)
        result = None
        if role == Qt.DisplayRole and orientation == Qt.Horizontal:
            try:
                result = ('#', 'thumb', 'name', 'count')[section]
            except IndexError:
                pass
        return result

    def data(self, index, role):
        result = None
        if not index.isValid():
            return result

        if role not in [Qt.DecorationRole, Qt.DisplayRole, Qt.CheckStateRole]:
            return super(UsersModel, self).data(index, role)
        row = index.row()
        col = index.column()
        record = self.record(row)
        if role == Qt.DecorationRole:
            if col == 1:
                url = str(record.value('profile_image_url').toString())
                result = imagesManager[url].pixmap(32)
        elif role == Qt.CheckStateRole:
            if col == 0:
                result = in2bool(record.value('is_active').toInt()[0])
        elif role == Qt.DisplayRole:
            if col == 1:
                pass
            elif col == 2:
                result = record.value('screen_name').toString()
            elif col == 3:
                result = record.value('statuses_count').toString()
        return result

    def setData(self, index, value, role):
        if role != Qt.CheckStateRole:
            return super(UsersModel, self).setData(index, value, role)
        #result = None
        row = index.row()
        col = index.column()
        record = self.record(row)
        if col == 0:
            log.info('CHECK %s', value.toInt()[0])
            is_active = in2bool(record.value('is_active').toInt()[0])
            setval = 1 if not is_active else 0
            id_user = record.value('id').toInt()[0]
            self.update_field(id_user, 'is_active', setval)
        return True

    def get(self, id_user, colname='id'):
        if colname not in ['id', 'screen_name', 'name']:
            raise ValueError('Invalid <<colname=%s>> must be <id> or <text>')
        fields = ','.join(self.fields)
        query = QSqlQuery(self.db)
        query.prepare("SELECT %s FROM users WHERE %s=:id_user" % (fields,
                                                                  colname))
        query.bindValue(':id_user', id_user)
        if not query.exec_():
            log.error('Cannot get user with %s <<%s>>\n\tError: %s',
                      colname, id_user, query.lastError().text())
            query.clear()
            return False, None
        if not query.first():
            log.error("Error (no next): %s", query.lastError().text())
            query.clear()
            return False, None
        record = self.record()
        idx = 0
        for f in self.fields:
            record.setValue(f, query.value(idx))
            idx += 1
        return True, record

    @transaction
    def add(self, user):
        log.info('Trying to insert user << %s >>', user)
        record = self.record()
        for key in self.fields:
            if key == 'is_active':
                value = 1
            else:
                value = getattr(user, key)
                if key == 'created_at':
                    value = value.isoformat()
            log.info(' - %s=<<%s>>', key, value)
            record.setValue(key, value)
        self.insertRecord(-1, record)
        if not self.submitAll():
            log.error('Cannot insert user: %s', user.name)
            self.revertAll()
            return False
        return True

    def update_field(self, id_user, field, value):
        qs = "UPDATE users SET %s = :value WHERE id=:id_user" % field
        log.info('Query: %s', qs)
        log.info(':id_user=%s, :value=%s', id_user, value)
        query = QSqlQuery(qs, self.db)
        query.prepare(qs)
        query.bindValue(':value', value)
        query.bindValue(':id_user', id_user)
        if not query.exec_():
            log.error('Exec fail: %s', query.lastError().text())
            query.clear()
            return False
        query.clear()
        self.updateSlot()
        return True

    @transaction
    def update(self, user):
        id_user = user.id
        fields = ['statuses_count', 'description', 'profile_image_url',
                 'screen_name']
        qs = "UPDATE users SET"
        for f in fields:
            qs += " %s=:%s," % (f, f)
        qs = qs[0:-1]
        qs += " WHERE id=:id_user"
        log.info("query: %s", qs)
        query = QSqlQuery(self.db)
        query.prepare(qs)
        for f in fields:
            log.info('bind :%s to %s', f, getattr(user, f))
            query.bindValue(':%s' % f, getattr(user, f))
        query.bindValue(':id_user', id_user)
        if not query.exec_():
            log.error("Cannot update user, Error: %s",
                      query.lastError().text())
            return False
        if not self.submitAll():
            log.error('Cannot submit new data: %s', query.lastError().text())
            query.clear()
            self.revertAll()
            return False
        query.clear()
        return True

    @transaction
    def is_empty(self):
        query = QSqlQuery(self.db)
        if not query.exec_("SELECT COUNT(*) from users"):
            log.error('Cannot count users\n\tError: %s',
                      query.lastError().text())
            return False
        if not query.first():
            log.error(query.lastError().text())
            return False
        count = query.value(0).toInt()
        if not count[1]:
            return False
        count = count[0]
        if count > 0:
            return False
        return True

    def list(self):
        for i in range(0, self.rowCount()):
            yield self.record(i)
