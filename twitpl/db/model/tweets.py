import logging
log = logging.getLogger('TwitPl:Model:Tweets')
log.setLevel(logging.ERROR)
from PyQt4.QtSql import QSqlQuery, QSqlTableModel
from twitpl.db.connect import transaction
from twitpl.media_resolver import media_resolver as mr, MediaURL
# from twitpl.media_resolver import MediaQualityEnum


class TweetsModel(QSqlTableModel):

    fields = ['id', 'text', 'source', 'retweet_count', 'created_at',
              'favorited', 'fts_hashtags', 'media_parsed']

    def __init__(self, parent=None, db=None):
        super(TweetsModel, self).__init__(parent, db)
        self.db = db
        self.setTable('tweets')
        self.setEditStrategy(QSqlTableModel.OnManualSubmit)
        if not self.select():
            log.error('TweetsModel Error: %s', self.lastError().text())

    @transaction
    def exists(self, tid):
        query = QSqlQuery(self.db)
        query.prepare("SELECT id FROM tweets WHERE id = :id")
        query.bindValue(':id', tid)
        if not query.exec_():
            log.error('exists error, %s', query.lastError().text())
            return False
        if not query.first():
            return False
        return True

    def add_hashtags(self, id_tweet, hashtags):
        from twitpl.db.init import dbo
        hashtagsModel = dbo.model.hashtags
        tweetsModel = dbo.model.tweets
        ids = hashtagsModel.add(hashtags)
        log.info('ids: %s', ids)
        tweetsModel.attach_hashtags(id_tweet, ids)

    def add_medias(self, user, id_tweet, urls):
        from twitpl.db.init import dbo
        mediasModel = dbo.model.medias
        tweetsModel = dbo.model.tweets
        ok = True
        for url in urls:
            media = MediaURL(url)
            if not mr.resolve(media):
                ok = False
                continue
            if mediasModel.add(media):
                ok = True
                ret, record = mediasModel.get(media.host, media.id)
                if ret:
                    id_media = record.value('id').toString()
                    if tweetsModel.attach_media(id_tweet, id_media):
                        pass
        return ok

    @transaction
    def add(self, user, status):
        if self.exists(status.id):
            log.info('tweets already saved <<%s>>', status.id)
            return True
        user_id = str(user.value('id').toString())
        hashtags = [h['text'] for h in status.entities['hashtags']]
        self.add_hashtags(status.id, hashtags)
        urls = [u['expanded_url'] or u['url'] for u in status.entities['urls']]
        media_parsed = self.add_medias(user, status.id, urls)
        log.info('Trying to insert tweet << %s >>', status)
        record = self.record()
        for key in self.fields:
            value = None
            if key == 'fts_hashtags':
                value = ' '.join(hashtags).strip()
            elif key == 'media_parsed':
                if media_parsed:
                    value = 1
                else:
                    value = 0
            else:
                value = getattr(status, key)
                if key == 'created_at':
                    value = value.isoformat()

            record.setValue(key, value)
            log.debug(' - %s: %s', key, value)
        record.setValue('id_user', user_id)
        self.insertRecord(-1, record)
        if not self.submitAll():
            log.error('Cannot insert tweet: %s', status.text)
            self.revertAll()
            return False
        return True

    @transaction
    def user_count(self, id_user):
        query = QSqlQuery(self.db)
        query.prepare("SELECT COUNT(*) from tweets WHERE id_user = :id_user")
        query.bindValue(':id_user', id_user)
        if not query.exec_():
            log.error("Exec fail: %s", query.lastError().text())
            return None
        if not query.first():
            log.error("No data: %s", query.lastError().text())
            return None
        count = query.value(0).toString()
        return int(count)

    @transaction
    def user_last_id(self, id_user):
        query = QSqlQuery(self.db)
        query.prepare("SELECT MAX(id) from tweets WHERE id_user = :id_user")
        query.bindValue(':id_user', id_user)
        if not query.exec_():
            log.error("Exec fail: %s", query.lastError().text())
            return None
        if not query.first():
            log.error("No data: %s", query.lastError().text())
            return None
        count = 0
        try:
            ret, _count = query.value(0).toInt()
            if ret:
                count = _count
        except ValueError as e:
            log.error('no tweet for this user ? (Error: %s)', e)
        return count

    @transaction
    def user_first_id(self, id_user):
        query = QSqlQuery(self.db)
        query.prepare("SELECT MIN(id) from tweets WHERE id_user = :id_user")
        query.bindValue(':id_user', id_user)
        if not query.exec_():
            log.error('user_first_id exec error, %s', query.lastError().text())
            return None
        if not query.first():
            log.error('user_first_id first error, %s',
                      query.lastError().text())
            return None
        count = query.value(0).toString()
        count = int(count)
        return count

    def attach_hashtag(self, id_tweet, id_hashtag):
        log.info('Attach hashtag: %s <- %s', id_tweet, id_hashtag)
        query = QSqlQuery(self.db)
        query.prepare("INSERT INTO tweet_hashtags (id_tweet, id_hashtag) " \
                      "VALUES (:id_tweet, :id_hashtag)")
        query.bindValue(':id_tweet', id_tweet)
        query.bindValue(':id_hashtag', id_hashtag)
        if not query.exec_():
            log.info('Query: %s', query.lastQuery())
            log.error('Exec Error: %s', query.lastError().text())
            query.clear()
            return False
        query.clear()
        return True

    def attach_hashtags(self, id_tweet, hashtags):
        ret = True
        for hashtag in hashtags:
            if not self.attach_hashtag(id_tweet, hashtag):
                ret = False
        return ret

    def attach_media(self, id_tweet, id_media):
        log.info('Attach media: %s <- %s', id_tweet, id_media)
        query = QSqlQuery(self.db)
        query.prepare("INSERT INTO tweet_medias (id_tweet, id_media) " \
                      "VALUES (:id_tweet, :id_media)")
        query.bindValue(':id_tweet', id_tweet)
        query.bindValue(':id_media', id_media)
        if not query.exec_():
            log.info('Query: %s', query.lastQuery())
            log.error('Exec Error: %s', query.lastError().text())
            query.clear()
            return None
        query.clear()
        return True

    def attach_medias(self, id_tweet, medias):
        ret = True
        for media_id in medias:
            if not self.attach_media(id_tweet, media_id):
                ret = False
        return ret
