import logging
log = logging.getLogger('TwitPl:Model:Medias')
log.setLevel(logging.ERROR)
import time

from PyQt4.QtSql import QSqlQuery, QSqlTableModel
from PyQt4.Qt import Qt
from PyQt4.QtGui import QSortFilterProxyModel
from twitpl.db.connect import transaction
from twitpl.utils import pretty_duration
from twitpl.images_manager import imagesManager


class MediasModel(QSqlTableModel):

    fields = ['id', 'host', 'title', 'extracted_id', 'thumbnail_url',
              'duration', 'page_url']

    def __init__(self, parent=None, db=None):
        super(MediasModel, self).__init__(parent, db)
        self.db = db
        self.setTable('medias')
        self.setEditStrategy(QSqlTableModel.OnManualSubmit)

    def updateSlot(self):
        pass

    @transaction
    def exists(self, tid):
        query = QSqlQuery(self.db)
        query.prepare("SELECT id FROM medias WHERE id = :id")
        query.bindValue(':id', tid)
        if not query.exec_():
            log.error(query.lastError().text())
            return False
        if not query.first():
            return False
        return True

    @transaction
    def add(self, media):
        log.info('Trying to insert media << %s >>', media)
        record = self.record()
        for key in self.fields:
            if key == 'id':
                continue
            if key == 'extracted_id':
                value = getattr(media, 'id')
            elif key == 'page_url':
                value = getattr(media, 'url')
            else:
                value = getattr(media, key)
            record.setValue(key, value)
            log.debug(' - %s: %s', key, value)
        self.insertRecord(-1, record)
        if not self.submitAll():
            log.error('Cannot insert media: %s\n\tError: %s',
                      media.title,
                      self.lastError().text())
            self.revertAll()
            return False
        return True

    def columnCount(self, _index):
        return 4

    def headerData(self, section, orientation, role):
        if role != Qt.DisplayRole:
            return super(MediasModel, self).headerData(
                                                    section, orientation, role)
        result = None
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            try:
                result = ('#', 'title', 'duration', 'provider')[section]
            except IndexError:
                pass
        return result

    def data(self, index, role):
        result = None
        if not index.isValid():
            return None
        if role != Qt.DecorationRole and role != Qt.DisplayRole:
            return super(MediasModel, self).data(index, role)
        row = index.row()
        col = index.column()
        record = self.record(row)
        if role == Qt.DecorationRole:
            if col == 0:
                url = str(record.value('thumbnail_url').toString())
                return imagesManager[url].pixmap(32)
        elif role == Qt.DisplayRole:
            if col == 0:
                pass
            elif col == 1:
                result = record.value('title').toString()
            elif col == 2:
                duration = 0
                try:
                    duration = record.value('duration').toInt()[0]
                except Exception as e:
                    log.error('Cannot convert <duration=%s>', e)
                time = duration
                result = pretty_duration(time / 1000)
            elif col == 3:
                result = record.value('host').toString()
        return result

    @transaction
    def get_id(self, host, extracted_id):
        query = QSqlQuery(self.db)
        query.prepare("SELECT id FROM medias WHERE "
                      "host=:host AND extracted_id=:extracted_id")
        query.bindValue(':host', host)
        query.bindValue(':extracted_id', extracted_id)
        if not query.exec_():
            log.error('Cannot get media <<host=%s, extracted_id=%s>>' \
                      '\n\tError: %s',
                      host, extracted_id,
                      query.lastError().text())
            query.clear()
            return False, None
        if not query.first():
            log.error("Error (no next): %s", query.lastError().text())
            query.clear()
            return False, None
        hid = query.value(0).toInt()
        if not hid[1]:
            log.error('Error: Cannot convert value to int')
            return False, None
        return True, hid[0]

    @transaction
    def get(self, host, extracted_id):
        fields = ','.join(self.fields)
        query = QSqlQuery(self.db)
        query.prepare("SELECT %s FROM medias WHERE "
                      "host=:host AND extracted_id=:extracted_id" % fields)
        query.bindValue(':host', host)
        query.bindValue(':extracted_id', extracted_id)
        if not query.exec_():
            log.error('Cannot get media <<host=%s, extracted_id=%s>>' \
                      '\n\tError: %s',
                      host, extracted_id,
                      query.lastError().text())
            query.clear()
            return False, None
        if not query.first():
            log.error("Error (no next): %s", query.lastError().text())
            query.clear()
            return False, None
        record = self.record()
        idx = 0
        for field in self.fields:
            record.setValue(field, query.value(idx))
            idx += 1
        return True, record


class MediasModelProxy(QSortFilterProxyModel):
    def __init__(self, parent=None, db=None):
        super(MediasModelProxy, self).__init__(parent)
        self.setFilterKeyColumn(-1)
