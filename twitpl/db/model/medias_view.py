import logging
log = logging.getLogger('TwitPl:Model:Media_view')
import time

from PyQt4.QtSql import QSqlQuery, QSqlQueryModel
from PyQt4.Qt import Qt
from twitpl.utils import pretty_duration
from twitpl.images_manager import imagesManager
from twitpl.db import log_error


class MediasViewModel(QSqlQueryModel):
    fields = ['id', 'host', 'title', 'extracted_id', 'thumbnail_url',
              'duration', 'page_url']

    sections = {
        '0': '#',
        '1': 'title',
        '2': 'duration',
        '3': 'host',
        '4': 'created_at'
    }

    def __init__(self, parent=None, db=None):
        super(MediasViewModel, self).__init__(parent)
        self.db = db
        self.setQuery()
        self.match = None

    def __set_query_no_match(self):
        fields = ''
        for f in self.fields:
                fields += ' m.%s,' % f
        fields = fields[0:-1]
        qs = 'SELECT %s, t.created_at FROM medias AS m ' \
        'INNER JOIN tweet_medias AS tm ON m.id = tm.id_media ' \
        'INNER JOIN tweets as t ON tm.id_tweet = t.id ' \
        'INNER JOIN users as u ON t.id_user = u.id ' \
        'WHERE u.is_active = 1 ' \
        'ORDER BY t.id DESC' % fields
        super(MediasViewModel, self).setQuery(QSqlQuery(qs, self.db))

    def __set_query_match(self, match):
        fields = ''
        for f in self.fields:
                fields += ' m.%s,' % f
        fields = fields[0:-1]
        query = QSqlQuery(self.db)
        #query.exec_("DELETE FROM vtweets")
        query.exec_("CREATE VIRTUAL TABLE vtweets USING fts3(id, hashtags)")
        log_error(query, "Deleting tweets fail")
        query.clear()
        query.exec_("INSERT INTO vtweets " \
                    "SELECT id, fts_hashtags AS hashtags FROM tweets " \
                    "WHERE media_parsed=1 " \
                    "AND id_user IN (SELECT id FROM users WHERE is_active=1)")
        log_error(query, "Insertings selected into virtual table fail")
        self.db.commit()
        query.clear()
        qs = "SELECT%s, t.created_at FROM medias AS m " \
        "JOIN tweet_medias AS tm " \
        "ON m.id = tm.id_media " \
        "JOIN tweets AS t " \
        "ON tm.id_tweet = t.id " \
        "AND t.media_parsed=1 " \
        "JOIN users AS u " \
        "ON t.id_user = u.id " \
        "WHERE t.id IN ( " \
        "SELECT t.id FROM vtweets AS t " \
        "WHERE t.hashtags MATCH :param) " \
        "AND u.is_active = 1 " \
        "ORDER BY t.id"
        qs = qs % (fields)
        log.info('Query: %s', qs)
        log.info('Match: %s', match)
        query = QSqlQuery(qs, self.db)
        query.bindValue(':param', match)
        query.exec_()
        log_error(query, 'set query fail')
        return super(MediasViewModel, self).setQuery(query)

    def setQuery(self, match=None):
        if match is None:
            match = self.match
        if match == '':
            match = None
        self.match = match
        if match is None:
            return self.__set_query_no_match()
        return self.__set_query_match(match)

    def columnCount(self, index):
        return len(self.sections)

    def headerData(self, section, orientation, role):
        if role != Qt.DisplayRole:
            return super(MediasViewModel, self).headerData(
                                                    section, orientation, role)
        result = None
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            num = str(section)
            if num in self.sections:
                result = self.sections[num]
        return result

    def data(self, index, role):
        result = None
        if not index.isValid():
            return None
        if role != Qt.DecorationRole and role != Qt.DisplayRole:
            return super(MediasViewModel, self).data(index, role)
        row = index.row()
        col = index.column()
        record = self.record(row)
        if role == Qt.DecorationRole:
            if col == 0:
                url = str(record.value('thumbnail_url').toString())
                return imagesManager[url].pixmap(32)
        elif role == Qt.DisplayRole:
            if col == 0:
                pass
            elif col == 1:
                result = record.value('title').toString()
            elif col == 2:
                duration = 0
                try:
                    duration = record.value('duration').toInt()[0]
                except Exception as e:
                    log.error('Cannot convert <duration=%s>', e)
                time = duration
                result = pretty_duration(time / 1000)
            elif col == 3:
                result = record.value('host').toString()
            elif col == 4:
                result = record.value('created_at').toString()
        return result
