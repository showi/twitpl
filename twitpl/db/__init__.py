import logging
log = logging.getLogger('TwitPl:Db')


def log_error(db, msg):
    error = db.lastError().text()
    if error == '':
        return False
    log.error('%s, %s', msg, db.lastError().text())
    return True
