from twitpl.db.dbo import Dbo
dbo = Dbo()
if not dbo.open():
            raise RuntimeError('Cannot open database: %s', dbo.path)
dbo.create_database()
