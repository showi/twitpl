CREATE TABLE tweet_hashtags (id_tweet INTEGER NOT NULL, id_hashtag INTEGER NOT NULL, UNIQUE(id_tweet, id_hashtag), FOREIGN KEY (id_tweet) REFERENCES tweets(id), FOREIGN KEY (id_hashtag) REFERENCES hashtags(id))
;
