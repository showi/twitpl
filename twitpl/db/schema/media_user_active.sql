CREATE VIEW media_user_active AS SELECT m.* FROM medias AS m
INNER JOIN tweet_medias AS tm ON m.id = tm.id_media
INNER JOIN tweets as t ON tm.id_tweet = t.id
INNER JOIN users as u ON t.id_user = u.id
WHERE u.is_active = 1
ORDER BY t.created_at DESC;
