CREATE TABLE tweet_medias (id_tweet INTEGER NOT NULL, id_media INTEGER NOT NULL, UNIQUE(id_tweet, id_media), FOREIGN KEY (id_tweet) REFERENCES tweets(id), FOREIGN KEY (id_media) REFERENCES media(id))
;
