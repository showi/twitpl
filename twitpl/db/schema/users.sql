CREATE TABLE users (id INTEGER PRIMARY KEY, name TEXT, description TEXT, statuses_count NUMERIC, profile_image_url TEXT, created_at VARCHAR NOT NULL, screen_name TEXT NOT NULL, is_active NUMERIC NOT NULL DEFAULT 1)
;
