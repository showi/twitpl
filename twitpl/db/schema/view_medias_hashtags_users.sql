CREATE VIEW media_hashtags_user AS SELECT m.* FROM medias AS m
INNER JOIN tweet_medias as tm
ON m.id = tm.id_media
INNER JOIN tweets as t
ON tm.id_tweet IN (
SELECT t.id FROM tweets AS t
INNER JOIN tweet_hashtags AS th
ON t.id = th.id_tweet
INNER JOIN hashtags AS h
ON th.id_hashtag = h.id
WHERE h.text in ('latin', 'jazz', 'newwave')
GROUP BY t.id )
INNER JOIN users AS u
ON t.id_user = u.id
WHERE u.is_active = 1
GROUP BY m.id