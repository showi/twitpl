import logging
log = logging.getLogger('TwitPl:Conf')

ck_key = 'authenticate/key'
ck_secret = 'authenticate/secret'


from PyQt4.QtCore import QSettings

scope = 'TwitPl'
section = 'Main'


class Conf(object):

    def __init__(self):
        self.settings = QSettings(scope, section)

    def __getitem__(self, path, default=None):
        return self.settings.value(path, default)

    def __setitem__(self, path, value):
        self.settings.setValue(path, value)

    def write(self):
        pass  # self.settings

    def get_as_string(self, path):
        ret = self[path]
        try:
            ret = str(ret.toString())  # Python 2.7
        except:
            pass  # Python 3
        return ret

    def get(self, path, asType='s'):
        if asType == 's':
            return self.get_as_string(path)
        else:
            raise ValueError('Unknown asType value <<%s>>', asType)

    def authentication_remove(self):
        for key in [ck_key, ck_secret]:
            self.settings.remove(key)

conf = Conf()
