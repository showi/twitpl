import logging
log = logging.getLogger('TwitPl:Player')
try:
    from PyQt4.phonon import Phonon
except Exception as e:
    log.critical('No Phonon backend\n%s', e)
from PyQt4.QtCore import QPersistentModelIndex, QModelIndex
from collections import namedtuple
from twitpl.utils import pretty_duration, trace
from twitpl.media_resolver import media_resolver as mr, MediaURL
from twitpl.media_resolver import MediaQualityEnum
from twitpl.db.init import dbo as dbObject
try:
    from urllib.parse import unquote_plus  # @UnresolvedImport @UnusedImport
except Exception as e:
    log.debug('Not Python3?: %s', e, trace(e))
    from urllib import unquote_plus  # @Reimport

MediaTuple = namedtuple('MediaTuple', 'id title url url_requested index')


qualities = ['default', 'best']


class Player(object):

    def __init__(self, widget):
        self.listeners = {}
        self.dbo = dbObject
        self.widget = widget
        self.quality = MediaQualityEnum.default
        self.title = ''
        self.current_media = None
        try:
            self.init(widget)
        except Exception as e:
            log.critical('Cannot init phonon object, %s\n%s', e, trace(e))

    def init(self, widget):
        mediaObject = Phonon.MediaObject(widget)
        self.audioOutput = Phonon.AudioOutput(Phonon.MusicCategory, widget)
        Phonon.createPath(mediaObject, self.audioOutput)
        mediaObject.setTickInterval(500)
        mediaObject.tick.connect(self.tock)
        self.videoWidget = Phonon.VideoWidget(widget)
        Phonon.createPath(mediaObject, self.videoWidget)
        mediaObject.stateChanged.connect(self.on_state_changed)
        mediaObject.aboutToFinish.connect(self.on_about_to_finish)
        mediaObject.bufferStatus.connect(self.on_buffer_status)
        mediaObject.setTransitionTime(-1)
        self.mediaObject = mediaObject

    def next(self, index):
        pass

    def attach(self, name, widget):
        self.listeners[name] = widget

    @property
    def title(self):
        return self._title

    @title.getter
    def title(self):
        return self._title

    @title.setter
    def title(self, v):
        self._title = v
        if 'title' in self.listeners:
            self.listeners['title'].setText(v)

    @property
    def quality(self):
        return self._quality

    @quality.getter
    def quality(self):
        return self._quality

    @quality.setter
    def quality(self, v):
        v = str(v).lower()
        if v == 'best':
            pass
        else:
            v = 'default'
        self._quality = v

    def tock(self, seconds):
        duration = pretty_duration(seconds / 1000)
        if 'duration' in self.listeners:
            self.listeners['duration'].setText(duration)

    def model_index(self, index):
        if index is None:
            index = self.selected_index()
        proxy = self.dbo.model.mediasProxy
        return proxy.mapToSource(index)

    def selected_index(self):
        if not 'viewWidget' in self.listeners:
            log.error('No viewWidget in listeneres')
            return False
        view = self.listeners['viewWidget']
        index = view.selectedIndexes()[0]
        try:
            view.setCurrentIndex(index.sibling(index.row() + 1, 0))
            index = view.selectedIndexes()[0]
        except:
            index = QPersistentModelIndex(0, 0)
        return index

    def get_media(self, _index):
        model = self.dbo.model.mediasViewModel
        index = self.model_index(_index)
        #self.selected_idx = index
        log.info('Playing row: %i', index.row())
        rec = model.record(index.row())
        url = str(rec.value('page_url').toString())
        media = MediaURL(url)
        media.quality = self.quality
        media = mr.resolve(media)
#         quvi.quality = self.quality
#         log.debug('Quality: %s', quvi.quality)
#         quvi.url = str(rec.value('page_url').toString())
        if not media:
            return None
        m = MediaTuple(
                       id=rec.value('id').toInt()[0],
                       title=str(rec.value('title').toString()),
                       url=media.current_stream.url,
                       url_requested=media.request_url,
                       index=':'.join([
                                       str(index.column()),
                                       str(index.row())
                                       ])
        )
        return m

    def get_media_source(self, _index):
        m = self.get_media(_index)
        if m is None:
            log.error('Could not play url')
            return None
        return m

    def play(self, _index=None):
        mediaObject = self.mediaObject
        if _index is None:
            return mediaObject.play()
        else:
            m = self.get_media_source(_index)
            if m is None:
                log.error("Can't get media source from index")
                return False
            self.clear_queue()
            url = self.enqueue(m.url)
            self.title = m.title
            index = QPersistentModelIndex(_index)
            self.index = index
            mediaObject.setCurrentSource(Phonon.MediaSource(url))
            self.stop()
            self.play()
        if not self.enqueue_next_media(index):
            log.error('Cannot enqueue next media')
        return True

    def clear_queue(self):
        self.mediaObject.clearQueue()

    def get_next_index(self, _index):
        if _index is None:
            return None
        try:
            index = QPersistentModelIndex(_index)
            return index.sibling(1, 0)
        except Exception as e:
            log.error('Cannot get next index, %s\n%s', e, trace(e))
        return None

    def enqueue_next_media(self, _index=None):
        cidx = self.get_next_index(_index)
        if cidx is None:
            return False
        m = self.get_media(cidx)
        self.enqueue(m.url)
        return True

    def enqueue(self, url):
        log.debug('Queuing url: %s', url)
        furl = unquote_plus(url)
        self.mediaObject.enqueue(Phonon.MediaSource(furl))
        return furl

    def setText(self, lst, txt):
        self.listeners[lst].setText(txt)

    def toggle_play(self):
        if self.is_playing():
            self.pause()
        else:
            self.play()

    def on_quality_changed(self, quality):
        quality = qualities[quality]
        log.debug('Changing quality to %s', quality)
        if self.quality == quality:
            log.debug('Same quality')
            return True
        self.quality = quality
        self.stop()
        return self.play(QModelIndex(self.index))

    def on_about_to_finish(self):
        self.enqueue_next_media(self.index)

    def on_state_changed(self, state):
        if not 'play_text' in self.listeners:
            return
        lst = self.listeners['play_text']
        if state == Phonon.StoppedState:
            lst.setText('play')
        elif state == Phonon.PlayingState:
            lst.setText('pause')
        elif state == Phonon.PausedState:
            lst.setText('play')
        elif state == Phonon.LoadingState:
            lst.setText('loading')
        elif state == Phonon.BufferingState:
            lst.setText('buffering')
        else:
            log.error('Unknown state: %s', state)

    def on_buffer_status(self, percent):
        if 'buffer_status' not in self.listeners:
            return True
        self.listeners['buffer_status'].setValue(percent)

    def stop(self):
        if self.is_playing():
            self.mediaObject.stop()

    def pause(self):
        if self.is_playing():
            self.mediaObject.pause()

    def state(self):
        return self.mediaObject.state()

    def is_playing(self):
        if self.state() == Phonon.PlayingState:
            return True
        return False

    def ui_volume_slider(self):
        return Phonon.VolumeSlider(self.audioOutput, self.widget)

    def ui_seek_slider(self):
        return Phonon.SeekSlider(self.mediaObject, self.widget)
