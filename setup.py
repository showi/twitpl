from setuptools import setup, find_packages

setup(
    name="twitpl",
    version="0.0.5",
    packages=find_packages(),
    scripts=[],
    install_requires=['tweepy'],
    package_data={},
    author="sho",
    author_email="",
    description="",
    license="GPL",
    keywords="",
    url="",
)
